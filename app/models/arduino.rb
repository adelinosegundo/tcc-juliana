class Arduino < ApplicationRecord
  validates :uid, :ip, :port, presence: true

  def turn_led_on
    request "digital/8/o"
    request "digital/8/1"
  end

  def turn_led_off
    request "digital/8/0"
  end  

  def request path
    HTTParty.get("http://#{self.ip}:#{self.port}/#{path}")
  end
end
