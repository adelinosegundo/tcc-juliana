json.extract! arduino, :id, :uid, :created_at, :updated_at
json.url arduino_url(arduino, format: :json)
