json.extract! api_v1_arduino, :id, :created_at, :updated_at
json.url api_v1_arduino_url(api_v1_arduino, format: :json)
