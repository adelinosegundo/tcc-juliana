class CreateArduinos < ActiveRecord::Migration[5.0]
  def change
    create_table :arduinos do |t|
      t.string :uid

      t.timestamps
    end
  end
end
