class AddIpAndPortToArduino < ActiveRecord::Migration[5.0]
  def change
    add_column :arduinos, :ip, :string
    add_column :arduinos, :port, :string
  end
end
