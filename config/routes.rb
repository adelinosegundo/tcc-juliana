Rails.application.routes.draw do
  resources :arduinos do
    member do
      post :turn_led_on
      post :turn_led_off
    end
  end
  namespace :api do
    namespace :v1 do
      resources :arduinos
    end
  end
end
